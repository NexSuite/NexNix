Warning: File extension 'md' is not supported
Warning: File extension 'md' is not supported
Warning: File extension 'md' is not supported
Warning: File extension 'ld' is not supported

Line counts for project /home/nexware/NexNix/src.
Generated by the Atom editor package Line-Count on May 10 2020 20:50.
Counts are in order of source, comments, and total.

Files
-----
    7   5   14  Libc/stdio/error.c
   11   5   19  Libc/stdio/io.c
   85   5   93  Libc/stdio/printf.c
   20   5   28  Libc/stdlib/abort.c
   36   5   48  Libc/stdlib/math.c
   32   5   43  Libc/string/memory.c
   95  11  126  Libc/string/string.c
   27   5   37  drivers/parallel/parallel.c
    6   5   15  drivers/parallel/parallel.h
   70   5   85  drivers/serial/serial.c
    5   5   13  drivers/serial/serial.h
   87   5  102  drivers/terminal/terminal.c
   15   5   24  drivers/terminal/terminal.h
   30   5   41  drivers/terminal/vga.c
   24   5   35  drivers/video/video.c
    4   5   13  drivers/video/video.h
   86   5  100  hal/hal-i386.h
   18   5   26  hal/hal.c
   47   5   59  hal/i386/cmos.c
   24   6   35  hal/i386/cpu.c
   38   5   47  hal/i386/gdt.c
   13   0   14  hal/i386/gdt_flush.asm
   41   5   52  hal/i386/idt.c
    4   0    6  hal/i386/idt_flush.asm
   19   5   29  hal/i386/include/cmos.h
    5   5   13  hal/i386/include/cpu.h
   19   5   30  hal/i386/include/gdt.h
   19   5   28  hal/i386/include/idt.h
   23   5   33  hal/i386/include/paging.h
   33   5   42  hal/i386/include/tss.h
   17   5   26  hal/i386/io.c
   90   5  105  hal/i386/irq.c
   48   0   51  hal/i386/irq_stubs.asm
  143   5  154  hal/i386/isr.c
  105   0  111  hal/i386/isr_stubs.asm
  161   5  184  hal/i386/memory.c
  198  14  254  hal/i386/paging.c
   19   0   23  hal/i386/paging_internal.asm
   36   5   50  hal/i386/pde.c
   31   6   43  hal/i386/pit.c
   28   5   40  hal/i386/pte.c
   99   5  120  hal/i386/rtc.c
   25   5   35  hal/i386/tss.c
    5   0    5  hal/i386/tss_flush.asm
   20   0   24  hello/hello.c
   14   5   21  include/assert.h
   14   5   22  include/iso646.h
    6   5   14  include/kernel/api.h
   17   5   30  include/kernel/driver_core.h
  172   5  200  include/kernel/elf.h
   21   5   31  include/kernel/init.h
   27   5   34  include/kernel/kernel.h
   78   5   93  include/kernel/multiboot.h
   82   5  102  include/kernel/system.h
   94   5  129  include/kernel/tasking.h
   26   5   35  include/kernel/terminal.h
   53   5   66  include/kernel/vfs.h
   13   0   18  include/stdarg.h
    7   6   17  include/stdbool.h
   18   8   34  include/stddef.h
   11   6   21  include/stdint.h
    8   5   18  include/stdio.h
   33   8   50  include/stdlib.h
   14   7   26  include/string.h
    6   5   15  include/sys/io.h
    4   5   12  include/sys/types.h
   11   0   13  include/va_list.h
   51  16   80  kernel/arch/i386/boot.asm
   54   6   69  kernel/driver/driver.c
   43   6   56  kernel/kernel/init.c
   23   8   33  kernel/kernel/kernel.c
    8   5   15  kernel/kernel/panic.c
  122   5  146  kernel/kernel/ustar.c
  121   5  134  kernel/kernel_memmgr/cache.c
  220   5  241  kernel/kernel_memmgr/heap.c
   29   5   40  kernel/kernel_memmgr/include/cache.h
   29   5   40  kernel/kernel_memmgr/include/heap.h
  100   6  114  kernel/proc/loader.c
   38   5   50  kernel/proc/proc_aux.c
  116   5  142  kernel/proc/queue.c
  274   5  290  kernel/proc/scheduler.c
   39   5   51  kernel/proc/sysapi.c
   48   5   63  object_manager/vfsys/vfsys.c

Directories
-----------
   286   41   371  Libc
   103   15   126  Libc/stdio
    56   10    76  Libc/stdlib
   127   16   169  Libc/string
   268   45   365  drivers
    33   10    52  drivers/parallel
    75   10    98  drivers/serial
   132   15   167  drivers/terminal
    28   10    48  drivers/video
  1394  121  1715  hal
  1290  111  1589  hal/i386
   118   30   175  hal/i386/include
    20    0    24  hello
   729  110  1001  include
   576   50   734  include/kernel
    10   10    27  include/sys
  1267   92  1501  kernel
    51   16    80  kernel/arch
    51   16    80  kernel/arch/i386
    54    6    69  kernel/driver
   196   24   250  kernel/kernel
   399   20   455  kernel/kernel_memmgr
    58   10    80  kernel/kernel_memmgr/include
   567   26   647  kernel/proc
    48    5    63  object_manager
    48    5    63  object_manager/vfsys

Types
-----
   245   16   290  asm
  2746  223  3329  c
  1021  175  1421  h

Total
-----
  4012  414  5040
