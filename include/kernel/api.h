/*
    api.h - contains kernel api calls
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#ifndef _KERNEL_API_H
#define _KERNEL_API_H

int sys_fork(int tid);
int sys_get_tid();
int sys_get_pid();

#endif
