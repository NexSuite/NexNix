/*
    video.c - contains VBE display output routines
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#include <stdint.h>
#include <stdio.h>
#include <kernel/kernel.h>
#include <kernel/system.h>
#include <kernel/multiboot.h>
#include <kernel/driver_core.h>
#include "video.h"

#define VIDEO_VIRT_BASE 0xB0000000

uint32_t* video_buffer = VIDEO_VIRT_BASE;

void video_data_init()
{
    uint32_t buffer_size = 800 * 600 * (32 / 8);
    for(int i = 0; i < buffer_size; i += 4096)
        map_address(get_kernel_directory(), VIDEO_VIRT_BASE + i, 0xFC000000 + i, PTE_PRESENT | PTE_WRITEABLE);
}

void video_init()
{
    set_status_handler(DRIVER_VIDEO, STATE_RUNNING, video_data_init);
}

void video_put_pixel(uint16_t x, uint16_t y, uint16_t color)
{
    uint32_t pixel_offset = y * 3200 + (x * (32 / 8));
    video_buffer[pixel_offset] = color;
}
