/*
    io.c - has libc io functions
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#include <stdint.h>
#include <kernel/system.h>
#include <sys/io.h>

void outb(uint16_t port, uint8_t val)
{
    out_internal(port, val);
}

uint8_t inb(uint16_t port)
{
    return in_internal(port);
}
