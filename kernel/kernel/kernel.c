/*
    kernel.c - contains the main kernel function
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#include <kernel/kernel.h>
#include <kernel/system.h>
#include <kernel/multiboot.h>
#include <kernel/terminal.h>
#include <kernel/init.h>
#include <stdio.h>
#include <stdlib.h>

void kernel_main(multiboot_info* bootinfo, uint32_t magic)
{
    int ret = init(bootinfo, magic);
    if(ret == GENERAL_ERROR)
    {
        PANIC("An error occurred during startup!");
        exit(GENERAL_ERROR);
    }
    if(ret == INVALID_MULTIBOOT)
    {
        PANIC("Invalid Multiboot!");
        exit(INVALID_MULTIBOOT);
    }
    serial_write_string("System running.");
    //printf("Welcome to NexNix!\n");
    //create_proc();
    //exit(EXIT_SUCCESS);
    for(;;);
}
