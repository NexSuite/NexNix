/*
    init.c - initializes system
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#include <stdint.h>
#include <stdio.h>
#include <kernel/kernel.h>
#include <kernel/init.h>
#include <kernel/system.h>
#include <kernel/tasking.h>
#include <kernel/multiboot.h>
#include <kernel/vfs.h>

void kernel_init_thread();

multiboot_info* info = 0;

int init(multiboot_info* bootinfo, uint32_t magic)
{
    info = bootinfo;
    start_driver_framework();
    if(magic != MULTIBOOT_BOOTLOADER_MAGIC)
        return INVALID_MULTIBOOT;
    printf("Starting NexNix...");
    multiboot_module* initrd = (multiboot_module*)bootinfo->moduleAddress;
    init_initrd(initrd[0].mod_start);
    load_initrd(initrd[0].mod_end - initrd[0].mod_start);
    if(hal_init(bootinfo, 0) == GENERAL_ERROR)
        return GENERAL_ERROR;
    if(bootinfo->moduleCount == 0)
        return INVALID_MULTIBOOT;
    init_kernel_cache();
    init_kernel_heap();
    scheduler_init();
    create_thread(kernel_init_thread, 1, 1, 1, 0, 0);
    enable();
    return SUCCESS;
}

vbe_mode_info* get_vbe_info()
{
    return (vbe_mode_info*)info->vbeModeInfo;
}

void kernel_init_thread()
{
    //sys_exit(2);
    vfs_file* file = vfs_open("/initrd/test.txt", 0);
    uint8_t* buffer = kmalloc(sizeof(char) * 4);
    vfs_read(file, buffer, 4);
    for(int i = 0; i < 4; i++)
        printf("%c", buffer[i]);
    for(;;) asm("pause");
}
