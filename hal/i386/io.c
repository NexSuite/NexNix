/*
    io.c - has low lever I/O functions
    Distributed with NexNix, Licensed under the GPL v3.0.
    See LICENSE.md
*/

#ifdef ARCH_X86
#include <stdint.h>
#include <stddef.h>
#include <kernel/kernel.h>

void out_internal(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

uint8_t in_internal(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                 : "=a"(ret)
                 : "Nd"(port) );
    return ret;
}

#endif
